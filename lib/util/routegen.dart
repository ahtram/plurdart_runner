import 'package:flutter/material.dart';
import 'package:plurdart_runner/screens/prototype.dart';
import 'package:plurdart_runner/screens/auth_ninja.dart';
import 'transparent_route.dart';

//Reference: https://www.youtube.com/watch?v=nyvwx7o277U

Route<dynamic> generateRoute(RouteSettings settings) {
  switch(settings.name) {
    case 'prototype':
      return new MaterialPageRoute(
          builder: (context) => Prototype(),
          settings: settings,
      );
    case 'prototype/auth_ninja':
      return new TransparentRoute(
          builder: (context) => new AuthNinja(settings.arguments),
          settings: settings,
      );
    default:
      return _errorRoute();
  }
}

//The default return Route.
Route<dynamic> _errorRoute() {
  return MaterialPageRoute(
    settings: RouteSettings(
      name: 'error'
    ),
    builder: (context) => Scaffold(
      appBar: null,
      body: SafeArea(
        child: Center(
          child: Text('Error!'),
        ),
      ),
    )
  );
}
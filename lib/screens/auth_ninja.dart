import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class AuthNinjaArgs {
  final String initUri;
  final Function onVerifierReceived;
  final Function onLoginError;

  final String loginUsername;
  final String loginPassword;

  AuthNinjaArgs(this.initUri, this.onVerifierReceived, this.onLoginError, this.loginUsername, this.loginPassword);
}

class AuthNinja extends StatefulWidget {
  final AuthNinjaArgs args;

  AuthNinja(this.args);

  @override
  _AuthNinjaState createState() => _AuthNinjaState();
}

class _AuthNinjaState extends State<AuthNinja> {
  _AuthNinjaState();

  // Use for control the webView to evaluate JS.
  WebViewController _controller;

  final String _getVerifierJS = 'document.getElementById("oauth_verifier").innerHTML;';
  final String _grantAccessJS = 'document.authorize.accept.value="1"; document.authorize.submit();';

  final String _tryGetTheErrorClassJS = 'document.getElementsByClassName("error")[0]';

  final String _fillUserNameInputJS = 'document.getElementsByName("username")[0].value = "%u"';
  final String _fillPasswordInputJS = 'document.getElementsByName("password")[0].value = "%p"';

  final String _submitFormJS = 'document.getElementsByClassName("login")[0].submit();';

  final String _plurkLoginUrlStartPattern = 'https://www.plurk.com/m/login';
  final String _plurkLoginAuthUrlStartPattern = 'https://www.plurk.com/m/authorize';
  final String _plurkLoginAuthDoneUrlStartPattern = 'https://www.plurk.com/OAuth/authorizeDone';

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: IgnorePointer(
        child: Material(
          color: Colors.transparent,
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Expanded(
                  child: Opacity(
                    opacity: 0.0,
                    child: WebView(
                      initialUrl: widget.args.initUri,
                      javascriptMode: JavascriptMode.unrestricted,
                      onWebViewCreated: (WebViewController controller) {
//                      print('onWebViewCreated got a controller');

                        // Store the WebView controller for later use.
                        _controller = controller;
                      },
                      onPageStarted: (url) {
//                      print('onPageStarted: ' + url);
                      },
                      onPageFinished: (url) {
//                      print('onPageFinished: ' + url);
                        if (url.startsWith(_plurkLoginUrlStartPattern)) {

                          // Try test if login error...by getting the error class.
                          _controller.evaluateJavascript(_tryGetTheErrorClassJS).then((result) {
//                          print('We got _tryGetTheErrorClassJS result: ' + result);
                            if (result != null && result != 'null') {
//                            print('We got an login error: ' + result);
                              // Leave this page.
                              _controller.clearCache().then((value) {
                                widget.args.onLoginError();
                              });
                            } else {
                              // Auto fill input field.
//                            print('No error. Starting to fill the form...');
                              _controller.evaluateJavascript(_fillUserNameInputJS.replaceAll('%u', widget.args.loginUsername)).then((value) {
                                _controller.evaluateJavascript(_fillPasswordInputJS.replaceAll('%p', widget.args.loginPassword)).then((value) {
                                  _controller.evaluateJavascript(_submitFormJS);
                                });
                              });
                            }
                          });
                        } else if (url.startsWith(_plurkLoginAuthUrlStartPattern)) {
                          //Press grant access.
//                        print('auto grant access...');
                          _controller.evaluateJavascript(_grantAccessJS);
                        } else if (url.startsWith(_plurkLoginAuthDoneUrlStartPattern)) {
//                        print('evaluating verifier... [' + _getVerifierJS + ']');
                          //Auth done , we should try read the verifier.
                          _controller.evaluateJavascript(_getVerifierJS).then((result) {
//                          print('evaluate verifier result: ' + result);
                            _controller.clearCache().then((value) {
                              widget.args.onVerifierReceived(result.replaceAll(RegExp('"'), ''));
                            });
                          });
                        } else {
                          //??? This is not a legal url...
                          //Treat this as a login error.
                          widget.args.onLoginError();
                        }
                      },
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
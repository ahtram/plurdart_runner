import 'dart:io';
import 'dart:math';
import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:oauth1/oauth1.dart';

import 'package:wakelock/wakelock.dart';
import 'package:plurdart/plurdart.dart';
import 'auth_ninja.dart';
import 'package:plurdart_runner/screens/busy.dart' as Busy;
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:pretty_json/pretty_json.dart';
import 'package:color_plus/color_plus.dart' as ColorPlus;
import 'message_dialog.dart' as MessageDialog;
import 'package:after_layout/after_layout.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:image_picker/image_picker.dart';

// Define our app key here.
String _appKey = 'AThyVHV5VndX';
String _appSecret = '9uEkKrUwewlJkpN51QcgdF4FwTwZe9K5';

class Prototype extends StatefulWidget {
  Prototype();
  @override
  _PrototypeState createState() => _PrototypeState();
}

class _PrototypeState extends State<Prototype> with AfterLayoutMixin {
  // My profile from the own profile API.
  Profile displayingProfile;

  // We need to cache this for display SnackBar. Stupid yeah?
  BuildContext scaffoldBodyContext;

  // For blur image filter.
  double _sigmaX = 50.0; // from 0-10
  double _sigmaY = 50.0; // from 0-10
  double _opacity = 0.25; // from 0-1.0

  // Testing cache stuffs.
  int plurkIdAdded;
  int responseIdAdded;

  @override
  void initState() {
    super.initState();
    // This will prevent my testing device from sleeping...
    Wakelock.toggle(on: true);
  }

  @override
  void afterFirstLayout(BuildContext context) {
    // Note that we need scaffoldBodyContext ready for display SnackBar.
    _tryRestoreClient();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Scaffold(
        appBar: null,
        body: Builder(
          builder: (scaffoldBuildContext) {
            // Cache the Scaffold build context for display SnackBar.
            scaffoldBodyContext = scaffoldBuildContext;
            return SafeArea(
              child: Container(
                decoration: BoxDecoration(
                  image: (displayingProfile == null)
                      ? (null)
                      : DecorationImage(
                          image: NetworkImage(
                              displayingProfile.userInfo.avatarBig),
                          fit: BoxFit.cover,
                        ),
                ),
                alignment: Alignment.center,
                child: BackdropFilter(
                  filter: ImageFilter.blur(sigmaX: _sigmaX, sigmaY: _sigmaY),
                  child: Container(
                    color: ColorPlus.Black.withOpacity(_opacity),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: ListView(children: <Widget>[
                        // The avatar place holder.
                        Center(
                          child: Container(
                            decoration: BoxDecoration(
                                border: Border.all(
                              width: 1.5,
                              color: Colors.grey,
                            )),
                            width: 200,
                            height: 200,
                            child: (displayingProfile == null)
                                ? (null)
                                : (FadeInImage.memoryNetwork(
                                    placeholder: kTransparentImage,
                                    image: displayingProfile.userInfo.avatarBig,
                                  )),
                          ),
                        ),

                        // Image filter stuffs.
                        Slider(
                          value: _sigmaX,
                          label: '_sigmaX',
                          divisions: 100,
                          onChanged: (value) {
                            setState(() {
                              _sigmaX = value;
                            });
                          },
                          min: 0,
                          max: 100,
                        ),

                        Slider(
                          value: _sigmaY,
                          label: '_sigmaY',
                          divisions: 100,
                          onChanged: (value) {
                            setState(() {
                              _sigmaY = value;
                            });
                          },
                          min: 0,
                          max: 100,
                        ),

                        Slider(
                          value: _opacity,
                          label: '_opacity',
                          divisions: 100,
                          onChanged: (value) {
                            setState(() {
                              _opacity = value;
                            });
                          },
                          min: 0,
                          max: 1,
                        ),

//                        ColoredTestButton('Test', _testSomething),
                        Text('Auth'),
                        ColoredTestButton('Login', _getAuthUriAndOpenAuthPage),

                        Text('Users'),
                        ColoredTestButton('UsersMe', _testUsersMe),
                        ColoredTestButton('UsersUpdate', _testUsersUpdate),
                        ColoredTestButton(
                            'UsersGetKarmaStats', _testUsersGetKarmaStats),

                        Text('Profile'),
                        ColoredTestButton(
                            'ProfileGetOwnProfile', _testProfileGetOwnProfile),
                        ColoredTestButton('ProfileGetPublicProfile',
                            _testProfileGetPublicProfile),

                        Text('Real time notifications'),
                        ColoredTestButton('RealtimeGetUserChannel',
                            _testRealtimeGetUserChannel),
                        ColoredTestButton('GetComet', _testGetComet),

                        Text('Polling'),
                        ColoredTestButton(
                            'PollingGetPlurks', _testPollingGetPlurks),
                        ColoredTestButton('PollingGetUnreadCount',
                            _testPollingGetUnreadCount),

                        Text('Timeline'),
                        ColoredTestButton(
                            'TimelineGetPlurk', _testTimelineGetPlurk),
                        ColoredTestButton(
                            'TimelineGetPlurks', _testTimelineGetPlurks),
                        ColoredTestButton('TimelineGetUnreadPlurks',
                            _testTimelineGetUnreadPlurks),
                        ColoredTestButton('TimelineGetPublicPlurks',
                            _testTimelineGetPublicPlurks),
                        ColoredTestButton(
                            'TimelinePlurkAdd', _testTimelinePlurkAdd),
                        ColoredTestButton(
                            'TimelinePlurkDelete', _testTimelinePlurkDelete),
                        ColoredTestButton(
                            'TimelinePlurkEdit', _testTimelinePlurkEdit),
                        ColoredTestButton(
                            'TimelineToggleComments', _testTimelineToggleComments),
                        ColoredTestButton(
                            'TimelineMutePlurks', _testTimelineMutePlurks),
                        ColoredTestButton(
                            'TimelineUnmutePlurks', _testTimelineUnmutePlurks),
                        ColoredTestButton(
                            'TimelineFavoritePlurks', _testTimelineFavoritePlurks),
                        ColoredTestButton(
                            'TimelineUnfavoritePlurks', _testTimelineUnfavoritePlurks),
                        ColoredTestButton(
                            'TimelineReplurk', _testTimelineReplurk),
                        ColoredTestButton(
                            'TimelineUnreplurk', _testTimelineUnreplurk),
                        ColoredTestButton(
                            'TimelineMarkAsRead', _testTimelineMarkAsRead),
                        ColoredTestButton(
                            'TimelineUploadPicture', _testTimelineUploadPicture),
                        ColoredTestButton(
                            'TimelineReportAbuse', _testTimelineReportAbuse),

                        Text('Responses'),
                        ColoredTestButton(
                            'ResponsesGet', _testResponsesGet),
                        ColoredTestButton(
                            'ResponsesResponseAdd', _testResponsesResponseAdd),
                        ColoredTestButton(
                            'ResponsesResponseDelete', _testResponsesResponseDelete),

                        Text('Friends and fans'),
                        ColoredTestButton(
                            'FriendsFansGetFriendsByOffset', _testFriendsFansGetFriendsByOffset),
                        ColoredTestButton(
                            'FriendsFansGetFansByOffset', _testFriendsFansGetFansByOffset),
                        ColoredTestButton(
                            'FriendsFansGetFollowingByOffset', _testFriendsFansGetFollowingByOffset),
                        ColoredTestButton(
                            'FriendsFansBecomeFriend', _testFriendsFansBecomeFriend),
                        ColoredTestButton(
                            'FriendsFansRemoveAsFriend', _testFriendsFansRemoveAsFriend),
                        ColoredTestButton(
                            'FriendsFansBecomeFan', _testFriendsFansBecomeFan),
                        ColoredTestButton(
                            'FriendsFansSetFollowing', _testFriendsFansSetFollowing),
                        ColoredTestButton(
                            'FriendsFansGetCompletion', _testFriendsFansGetCompletion),

                        Text('Alerts'),
                        ColoredTestButton(
                            'AlertsGetActive', _testAlertsGetActive),
                        ColoredTestButton(
                            'AlertsGetHistory', _testAlertsGetHistory),
                        ColoredTestButton(
                            'AlertsAddAsFan', _testAlertsAddAsFan),
                        ColoredTestButton(
                            'AlertsAddAllAsFan', _testAlertsAddAllAsFan),
                        ColoredTestButton(
                            'AlertsAddAllAsFriends', _testAlertsAddAllAsFriends),
                        ColoredTestButton(
                            'AlertsAddAsFriend', _testAlertsAddAsFriend),
                        ColoredTestButton(
                            'AlertsDenyFriendship', _testAlertsDenyFriendship),
                        ColoredTestButton(
                            'AlertsRemoveNotification', _testAlertsRemoveNotification),

                        Text('Search'),
                        ColoredTestButton(
                            'PlurkSearchSearch', _testPlurkSearchSearch),
                        ColoredTestButton(
                            'UserSearchSearch', _testUserSearchSearch),

                        Text('Emoticons'),
                        ColoredTestButton(
                            'EmoticonsGet', _testEmoticonsGet),

                        Text('Blocks'),
                        ColoredTestButton(
                            'BlocksGet', _testBlocksGet),
                        ColoredTestButton(
                            'BlocksBlock', _testBlocksBlock),
                        ColoredTestButton(
                            'BlocksUnblock', _testBlocksUnblock),

                        Text('Cliques'),
                        ColoredTestButton(
                            'CliquesGetCliques', _testCliquesGetCliques),
                        ColoredTestButton(
                            'CliquesGetClique', _testCliquesGetClique),
                        ColoredTestButton(
                            'CliquesCreateClique', _testCliquesCreateClique),
                        ColoredTestButton(
                            'CliquesRenameClique', _testCliquesRenameClique),
                        ColoredTestButton(
                            'CliquesAdd', _testCliquesAdd),
                        ColoredTestButton(
                            'CliquesRemove', _testCliquesRemove),

                        Text('OAuth Utilities'),
                        ColoredTestButton(
                            'CheckToken', _testCheckToken),
                        ColoredTestButton(
                            'ExpireToken', _testExpireToken),
                        ColoredTestButton(
                            'CheckTime', _testCheckTime),
                        ColoredTestButton(
                            'Echo', _testEcho),

                      ]),
                    ),
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    Wakelock.toggle(on: false);
  }

  _showMessage(String msg) {
    MessageDialog.show(context, msg);
  }

  _showSnackBarMessage(String msg) {
    if (scaffoldBodyContext != null) {
      Scaffold.of(scaffoldBodyContext).showSnackBar(SnackBar(
        content: Text(msg),
      ));
    } else {
      print('Cannot _showSnackBarMessage because scaffoldBodyContext is null!');
    }
  }

  _updateDisplayingProfile(Profile profile) {
    setState(() {
      displayingProfile = profile;
    });
  }

  //--

  //This will try setup client use exist token/secret
  _tryRestoreClient() async {
    print('[_tryRestoreClient]');
    // show busy.
    Busy.show(context);

    FlutterSecureStorage storage = FlutterSecureStorage();

    // Test read the stuffs...
    String readToken = await storage.read(key: 'accessToken');
    if (readToken?.isEmpty ?? true) {
      print('accessToken not found.');
    } else {
      print('read accessToken [' + readToken + ']');
    }

    String readSecret = await storage.read(key: 'tokenSecret');
    if (readSecret?.isEmpty ?? true) {
      print('tokenSecret not exist!');
    } else {
      print('read tokenSecret [' + readSecret + ']');
    }

    // Test these credentials.
    if (!(readToken?.isEmpty ?? true) && !(readSecret?.isEmpty ?? true)) {
      // setup client.
      setupClient(_appKey, _appSecret, readToken, readSecret);

      // Test an API call to see if the client still work...
      CheckedExpiredToken checkedExpiredToken = await checkToken();
      if (checkedExpiredToken != null) {
        _showSnackBarMessage('[Credential restored. Plurdart has been setup]');
        print('[Credential restored. Plurdart has been setup]');
      } else {
        _showSnackBarMessage('[Credential expired. Re-Auth is required.]');
        print('[Credential expired. Re-Auth is required.]');
      }
    } else {
      _showSnackBarMessage('[Cannot restore credential. Auth is required.]');
      print('[Cannot restore credential. Auth is required.]');
    }

    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
  }

//  void _testSomething() async {
//    List<int> testList = [1, 2, 3, 456, 7890];
//    // Test print an int array.
//    print(testList.toString().replaceAll(' ', ''));
//  }

  void _getAuthUriAndOpenAuthPage() async {
    Busy.show(context);
    initialAuth(_appKey, _appSecret);
    String uri = await tryGetAuthUri();
    print('Plurdart tryGetAuthUri return [' + uri + ']');
    Navigator.pushNamed(context, 'prototype/auth_ninja',
        arguments: new AuthNinjaArgs(uri, _onVerifierReceived, _onLoginError, '123456', '654321'));
  }

  void _onVerifierReceived(String verifier) async {
    // Close the authWebView.
    print('Got verifier: ' + verifier);
    // We should load use dash board here...
//    Navigator.popUntil(context, (route) => route.isFirst);
//    Busy.show(context);

    // Get access token.
    Credentials credentials = await tryGetCredentials(verifier);

    FlutterSecureStorage storage = FlutterSecureStorage();

    // Try store access token and secret.
    await storage.write(key: 'accessToken', value: credentials.token);
    await storage.write(key: 'tokenSecret', value: credentials.tokenSecret);

    // We should load use dash board here...
//    Navigator.pop(context);
    Navigator.popUntil(context, (route) => route.isFirst);

    _showSnackBarMessage('AccessToken[' +
        credentials.token +
        '] TokenSecret[' +
        credentials.tokenSecret +
        ']');
  }

  _onLoginError() async {
    Navigator.popUntil(context, (route) => route.isFirst);
    _showSnackBarMessage('Login Error!');
  }

  _testUsersMe() async {
    //Show busy screen.
    Busy.show(context);
    User user = await usersMe();
    //Remove the busy screen.
    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
    if (user != null) {
      _showMessage(prettyJson(user.toJson()));
    }
  }

  _testUsersUpdate() async {
    //Show busy screen.
    Busy.show(context);
    User user = await usersUpdate(UsersUpdate());
    //Remove the busy screen.
    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
    if (user != null) {
      _showMessage(prettyJson(user.toJson()));
    }
  }

  _testUsersGetKarmaStats() async {
    //Show busy screen.
    Busy.show(context);
    KarmaStats karmaStats = await usersGetKarmaStats();
    //Remove the busy screen.
    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
    if (karmaStats != null) {
      //Got a return data.
      _showMessage(prettyJson(karmaStats.toJson()));
    }
  }

  _testProfileGetOwnProfile() async {
    //Show busy screen.
    Busy.show(context);
    Profile profile =
        await profileGetOwnProfile(ProfileGetOwnProfile());
    //Remove the busy screen.
    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
    if (profile != null) {
      //Got a return data.
      _showMessage(prettyJson(profile.toJson()));
    }
    _updateDisplayingProfile(profile);
  }

  _testProfileGetPublicProfile() async {
    //Show busy screen.
    Busy.show(context);
    Profile profile =
        await profileGetPublicProfile(ProfileGetPublicProfile(
      userId: 'devel1337', //the test account.
      nickName: '',
    ));
    //Remove the busy screen.
    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
    if (profile != null) {
      //Got a return data.
      _showMessage(prettyJson(profile.toJson()));
    }
    _updateDisplayingProfile(profile);
  }

  _testRealtimeGetUserChannel() async {
    //Show busy screen.
    Busy.show(context);
    UserChannel userChannel = await realtimeGetUserChannel();
    //Remove the busy screen.
    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
    if (userChannel != null) {
      //Got a return data.
      _showMessage(prettyJson(userChannel.toJson()));
    }
  }

  _testGetComet() async {
    //Show busy screen.
    Busy.show(context);
    Comet comet = await getComet();
    //Remove the busy screen.
    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
    if (comet != null) {
      //Got a return data.
      _showMessage(prettyJson(comet.toJson()));
    }
  }

  _testPollingGetPlurks() async {
    //Show busy screen.
    Busy.show(context);
    Plurks plurks = await pollingGetPlurks(PollingGetPlurks(
      offset: DateTime.parse('2020-06-20T21:55:34'),
    ));
    //Remove the busy screen.
    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
    if (plurks != null) {
      //Got a return data.
      _showMessage(prettyJson(plurks.toJson()));
    }
  }

  _testPollingGetUnreadCount() async {
    //Show busy screen.
    Busy.show(context);
    UnreadCount unreadCount = await pollingGetUnreadCount();
    //Remove the busy screen.
    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
    if (unreadCount != null) {
      //Got a return data.
      _showMessage(prettyJson(unreadCount.toJson()));
    }
  }

  _testTimelineGetPlurk() async {
    //Show busy screen.
    Busy.show(context);
    PlurkWithUser plurkWithUser =
        await timelineGetPlurk(TimelineGetPlurk(
      plurkId: 1445917780,
    ));
    //Remove the busy screen.
    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
    if (plurkWithUser != null) {
      //Got a return data.
      _showMessage(prettyJson(plurkWithUser.toJson()));
    }
  }

  _testTimelineGetPlurks() async {
    //Show busy screen.
    Busy.show(context);
    Plurks plurks = await timelineGetPlurks(
        TimelineGetPlurks(offset: DateTime.parse('2020-07-10T21:55:34')));
    //Remove the busy screen.
    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
    if (plurks != null) {
      //Got a return data.
      _showMessage(prettyJson(plurks.toJson()));
    }
  }

  _testTimelineGetUnreadPlurks() async {
    //Show busy screen.
    Busy.show(context);
    Plurks plurks = await timelineGetUnreadPlurks(
        TimelineGetPlurks(offset: DateTime.parse('2020-07-10T21:55:34')));
    //Remove the busy screen.
    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
    if (plurks != null) {
      //Got a return data.
      _showMessage(prettyJson(plurks.toJson()));
    }
  }

  _testTimelineGetPublicPlurks() async {
    //Show busy screen.
    Busy.show(context);
    Plurks plurks = await timelineGetPublicPlurks(
        TimelineGetPublicPlurks(userId: 4670999));
    //Remove the busy screen.
    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
    if (plurks != null) {
      //Got a return data.
      _showMessage(prettyJson(plurks.toJson()));
    }
  }

  _testTimelinePlurkAdd() async {
    //Show busy screen.
    Busy.show(context);
    int randomNum = Random().nextInt(99999);
    Plurk plurk = await timelinePlurkAdd(TimelinePlurkAdd(
        content: 'hello!' + randomNum.toString(), qualifier: Qualifier.Says));
    //Remove the busy screen.
    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
    if (plurk != null) {
      //Got a return data.
      plurkIdAdded = plurk.plurkId;
      _showMessage(prettyJson(plurk.toJson()));
    }
  }

  _testTimelinePlurkDelete() async {
    //Show busy screen.
    Busy.show(context);
    Base base = await timelinePlurkDelete(
        TimelinePlurkDelete(plurkId: plurkIdAdded));
    //Remove the busy screen.
    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
    if (base != null) {
      //Got a return data.
      _showMessage(prettyJson(base.toJson()));
    }
  }

  _testTimelinePlurkEdit() async {
    //Show busy screen.
    Busy.show(context);
    int randomNum = Random().nextInt(99999);
    Plurk plurk = await timelinePlurkEdit(TimelinePlurkEdit(
      plurkId: plurkIdAdded,
      content: 'hello!' + randomNum.toString(),
    ));
    //Remove the busy screen.
    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
    if (plurk != null) {
      //Got a return data.
      _showMessage(prettyJson(plurk.toJson()));
    }
  }

  _testTimelineToggleComments() async {
    //Show busy screen.
    Busy.show(context);
    ToggleComments toggleComments = await timelineToggleComments(TimelineToggleComments(
      plurkId: plurkIdAdded,
      noComment: Comment.Allow,
    ));
    //Remove the busy screen.
    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
    if (toggleComments != null) {
      //Got a return data.
      _showMessage(prettyJson(toggleComments.toJson()));
    }
  }

  _testTimelineMutePlurks() async {
    //Show busy screen.
    Busy.show(context);
    Base base = await timelineMutePlurks(PlurkIDs(
      ids: [1440720539,1438447762]
    ));
    //Remove the busy screen.
    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
    if (base != null) {
      //Got a return data.
      _showMessage(prettyJson(base.toJson()));
    }
  }

  _testTimelineUnmutePlurks() async {
    //Show busy screen.
    Busy.show(context);
    Base base = await timelineUnmutePlurks(PlurkIDs(
        ids: [1440720539,1438447762]
    ));
    //Remove the busy screen.
    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
    if (base != null) {
      //Got a return data.
      _showMessage(prettyJson(base.toJson()));
    }
  }

  _testTimelineFavoritePlurks() async {
    //Show busy screen.
    Busy.show(context);
    Base base = await timelineFavoritePlurks(PlurkIDs(
        ids: [1440720539,1438447762]
    ));
    //Remove the busy screen.
    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
    if (base != null) {
      //Got a return data.
      _showMessage(prettyJson(base.toJson()));
    }
  }

  _testTimelineUnfavoritePlurks() async {
    //Show busy screen.
    Busy.show(context);
    Base base = await timelineUnfavoritePlurks(PlurkIDs(
        ids: [1440720539,1438447762]
    ));
    //Remove the busy screen.
    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
    if (base != null) {
      //Got a return data.
      _showMessage(prettyJson(base.toJson()));
    }
  }

  _testTimelineReplurk() async {
    //Show busy screen.
    Busy.show(context);
    Base base = await timelineReplurk(PlurkIDs(
        ids: [1440720539,1438447762]
    ));
    //Remove the busy screen.
    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
    if (base != null) {
      //Got a return data.
      _showMessage(prettyJson(base.toJson()));
    }
  }

  _testTimelineUnreplurk() async {
    //Show busy screen.
    Busy.show(context);
    Base base = await timelineUnreplurk(PlurkIDs(
        ids: [1440720539,1438447762]
    ));
    //Remove the busy screen.
    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
    if (base != null) {
      //Got a return data.
      _showMessage(prettyJson(base.toJson()));
    }
  }

  _testTimelineMarkAsRead() async {
    //Show busy screen.
    Busy.show(context);
    Base base = await timelineMarkAsRead(TimelineMarkAsRead(
      ids: [1440720539,1438447762],
      notePosition: true
    ));
    //Remove the busy screen.
    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
    if (base != null) {
      //Got a return data.
      _showMessage(prettyJson(base.toJson()));
    }
  }

  _testTimelineUploadPicture() async {
    //Show busy screen.
    Busy.show(context);

    ImagePicker picker = ImagePicker();
    PickedFile pickedFile = await picker.getImage(source: ImageSource.gallery);

    if (pickedFile != null) {
      UploadPicture uploadPicture = await timelineUploadPicture(TimelineUploadPicture(
        image: File(pickedFile.path),
      ));

      //Remove the busy screen.
      Navigator.popUntil(context, (route) {
        return (route.settings.name == 'prototype');
      });
      if (uploadPicture != null) {
        //Got a return data.
        _showMessage(prettyJson(uploadPicture.toJson()));
      }
    } else {
      //Remove the busy screen.
      Navigator.popUntil(context, (route) {
        return (route.settings.name == 'prototype');
      });

      _showMessage('Oops! pickedFile is null?!');
    }
  }

  _testTimelineReportAbuse() async {
    //Show busy screen.
    Busy.show(context);
    Base base = await timelineReportAbuse(TimelineReportAbuse(
      plurkId: plurkIdAdded,
      abuseCategory: AbuseCategory.Others
    ));
    //Remove the busy screen.
    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
    if (base != null) {
      //Got a return data.
      _showMessage(prettyJson(base.toJson()));
    }
  }

  _testResponsesGet() async {
    //Show busy screen.
    Busy.show(context);
    Responses responses = await responsesGet(ResponsesGet(
        plurkId: 1441397950,
    ));
    //Remove the busy screen.
    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
    if (responses != null) {
      //Got a return data.
      _showMessage(prettyJson(responses.toJson()));
    }
  }

  _testResponsesResponseAdd() async {
    //Show busy screen.
    Busy.show(context);
    Response response = await responsesResponseAdd(ResponsesResponseAdd(
      plurkId: 1441397950,
      content: 'nice',
      qualifier: Qualifier.Says,
    ));
    //Remove the busy screen.
    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
    if (response != null) {
      //Got a return data.
      responseIdAdded = response.id;
      _showMessage(prettyJson(response.toJson()));
    }
  }

  _testResponsesResponseDelete() async {
    //Show busy screen.
    Busy.show(context);
    Base base = await responsesResponseDelete(ResponsesResponseDelete(
      plurkId: 1441397950,
      responseId: responseIdAdded,
    ));
    //Remove the busy screen.
    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
    if (base != null) {
      //Got a return data.
      _showMessage(prettyJson(base.toJson()));
    }
  }

  _testFriendsFansGetFriendsByOffset() async {
    //Show busy screen.
    Busy.show(context);
    List<User> users = await friendsFansGetFriendsByOffset(FriendsFansGetFriendsFansByOffset(
      userId: 14388927,
    ));
    //Remove the busy screen.
    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
    if (users != null) {
      //Got a return data.
      _showMessage('users.length: ' + users.length.toString());
    }
  }

  _testFriendsFansGetFansByOffset() async {
    //Show busy screen.
    Busy.show(context);
    List<User> users = await friendsFansGetFansByOffset(FriendsFansGetFriendsFansByOffset(
      userId: 14388927,
    ));
    //Remove the busy screen.
    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
    if (users != null) {
      //Got a return data.
      _showMessage('users.length: ' + users.length.toString());
    }
  }

  _testFriendsFansGetFollowingByOffset() async {
    //Show busy screen.
    Busy.show(context);
    List<User> users = await friendsFansGetFollowingByOffset(FriendsFansGetFollowingByOffset(
    ));
    //Remove the busy screen.
    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
    if (users != null) {
      //Got a return data.
      _showMessage('users.length: ' + users.length.toString());
    }
  }

  _testFriendsFansBecomeFriend() async {
    //Show busy screen.
    Busy.show(context);
    Base base = await friendsFansBecomeFriend(FriendsFansBecomeRemoveAsFriend(
      friendId: 4670999,
    ));
    //Remove the busy screen.
    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
    if (base != null) {
      //Got a return data.
      _showMessage(prettyJson(base.toJson()));
    }
  }

  _testFriendsFansRemoveAsFriend() async {
    //Show busy screen.
    Busy.show(context);
    Base base = await friendsFansRemoveAsFriend(FriendsFansBecomeRemoveAsFriend(
      friendId: 4670999,
    ));
    //Remove the busy screen.
    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
    if (base != null) {
      //Got a return data.
      _showMessage(prettyJson(base.toJson()));
    }
  }

  _testFriendsFansBecomeFan() async {
    //Show busy screen.
    Busy.show(context);
    Base base = await friendsFansBecomeFan(FriendsFansBecomeFan(
      fanId: 4670999,
    ));
    //Remove the busy screen.
    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
    if (base != null) {
      //Got a return data.
      _showMessage(prettyJson(base.toJson()));
    }
  }

  _testFriendsFansSetFollowing() async {
    //Show busy screen.
    Busy.show(context);
    Base base = await friendsFansSetFollowing(FriendsFansSetFollowing(
      userId: 4670999,
      follow: false,
    ));
    //Remove the busy screen.
    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
    if (base != null) {
      //Got a return data.
      _showMessage(prettyJson(base.toJson()));
    }
  }

  _testFriendsFansGetCompletion() async {
    //Show busy screen.
    Busy.show(context);
    Map<String, Completion> map = await friendsFansGetCompletion();
    //Remove the busy screen.
    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
    if (map != null) {
      //Got a return data.
      _showMessage(prettyJson(map));
    }
  }

  _testAlertsGetActive() async {
    //Show busy screen.
    Busy.show(context);
    List<Alert> alerts = await alertsGetActive();
    //Remove the busy screen.
    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
    if (alerts != null) {
      //Got a return data.
      _showMessage('users.length: ' + alerts.length.toString());
    }
  }

  _testAlertsGetHistory() async {
    //Show busy screen.
    Busy.show(context);
    List<Alert> alerts = await alertsGetHistory();
    //Remove the busy screen.
    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
    if (alerts != null) {
      //Got a return data.
      _showMessage('users.length: ' + alerts.length.toString());
    }
  }

  _testAlertsAddAsFan() async {
    //Show busy screen.
    Busy.show(context);
    Base base = await alertsAddAsFan(UserID(
      userId: 4670999,
    ));
    //Remove the busy screen.
    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
    if (base != null) {
      //Got a return data.
      _showMessage(prettyJson(base.toJson()));
    }
  }

  _testAlertsAddAllAsFan() async {
    //Show busy screen.
    Busy.show(context);
    Base base = await alertsAddAllAsFan();
    //Remove the busy screen.
    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
    if (base != null) {
      //Got a return data.
      _showMessage(prettyJson(base.toJson()));
    }
  }

  _testAlertsAddAllAsFriends() async {
    //Show busy screen.
    Busy.show(context);
    Base base = await alertsAddAllAsFriends();
    //Remove the busy screen.
    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
    if (base != null) {
      //Got a return data.
      _showMessage(prettyJson(base.toJson()));
    }
  }

  _testAlertsAddAsFriend() async {
    //Show busy screen.
    Busy.show(context);
    Base base = await alertsAddAsFriend(UserID(
      userId: 4670999,
    ));
    //Remove the busy screen.
    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
    if (base != null) {
      //Got a return data.
      _showMessage(prettyJson(base.toJson()));
    }
  }

  _testAlertsDenyFriendship() async {
    //Show busy screen.
    Busy.show(context);
    Base base = await alertsDenyFriendship(UserID(
      userId: 4670999,
    ));
    //Remove the busy screen.
    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
    if (base != null) {
      //Got a return data.
      _showMessage(prettyJson(base.toJson()));
    }
  }

  _testAlertsRemoveNotification() async {
    //Show busy screen.
    Busy.show(context);
    Base base = await alertsRemoveNotification(UserID(
      userId: 4670999,
    ));
    //Remove the busy screen.
    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
    if (base != null) {
      //Got a return data.
      _showMessage(prettyJson(base.toJson()));
    }
  }

  _testPlurkSearchSearch() async {
    //Show busy screen.
    Busy.show(context);
    PlurkSearch plurkSearch = await plurkSearchSearch(Search(
      query: 'hello',
    ));
    //Remove the busy screen.
    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
    if (plurkSearch != null) {
      //Got a return data.
      _showMessage(prettyJson(plurkSearch.toJson()));
    }
  }

  _testUserSearchSearch() async {
    //Show busy screen.
    Busy.show(context);
    UserSearch userSearch = await userSearchSearch(Search(
      query: 'hello',
    ));
    //Remove the busy screen.
    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
    if (userSearch != null) {
      //Got a return data.
      _showMessage(prettyJson(userSearch.toJson()));
    }
  }

  _testEmoticonsGet() async {
    //Show busy screen.
    Busy.show(context);
    Emoticons emoticons = await emoticonsGet();
    //Remove the busy screen.
    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
    if (emoticons != null) {
      //Got a return data.
      _showMessage(prettyJson(emoticons.toJson()));
    }
  }

  _testBlocksGet() async {
    //Show busy screen.
    Busy.show(context);
    Blocks blocks = await blocksGet();
    //Remove the busy screen.
    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
    if (blocks != null) {
      //Got a return data.
      _showMessage(prettyJson(blocks.toJson()));
    }
  }

  _testBlocksBlock() async {
    //Show busy screen.
    Busy.show(context);
    Base base = await blocksBlock(UserID(
      userId: 4670999,
    ));
    //Remove the busy screen.
    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
    if (base != null) {
      //Got a return data.
      _showMessage(prettyJson(base.toJson()));
    }
  }

  _testBlocksUnblock() async {
    //Show busy screen.
    Busy.show(context);
    Base base = await blocksUnblock(UserID(
      userId: 4670999,
    ));
    //Remove the busy screen.
    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
    if (base != null) {
      //Got a return data.
      _showMessage(prettyJson(base.toJson()));
    }
  }

  _testCliquesGetCliques() async {
    //Show busy screen.
    Busy.show(context);
    List<String> cliques = await cliquesGetCliques();
    //Remove the busy screen.
    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
    if (cliques != null) {
      //Got a return data.
      _showMessage(cliques.toString());
    }
  }

  _testCliquesGetClique() async {
    //Show busy screen.
    Busy.show(context);
    List<User> users = await cliquesGetClique(CliquesGetCreateClique(
      cliqueName: 'Classmates'
    ));
    //Remove the busy screen.
    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
    if (users != null) {
      //Got a return data.
      _showMessage('users.length: ' + users.length.toString());
    }
  }

  _testCliquesCreateClique() async {
    //Show busy screen.
    Busy.show(context);
    Base base = await cliquesCreateClique(CliquesGetCreateClique(
      cliqueName: 'abc'
    ));
    //Remove the busy screen.
    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
    if (base != null) {
      //Got a return data.
      _showMessage(prettyJson(base.toJson()));
    }
  }

  _testCliquesRenameClique() async {
    //Show busy screen.
    Busy.show(context);
    Base base = await cliquesRenameClique(CliquesRenameClique(
      cliqueName: 'abc',
      newName: 'def'
    ));
    //Remove the busy screen.
    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
    if (base != null) {
      //Got a return data.
      _showMessage(prettyJson(base.toJson()));
    }
  }

  _testCliquesAdd() async {
    //Show busy screen.
    Busy.show(context);
    Base base = await cliquesAdd(CliquesAddRemove(
      cliqueName: 'ghi',
      userId: 4670999,
    ));
    //Remove the busy screen.
    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
    if (base != null) {
      //Got a return data.
      _showMessage(prettyJson(base.toJson()));
    }
  }

  _testCliquesRemove() async {
    //Show busy screen.
    Busy.show(context);
    Base base = await cliquesRemove(CliquesAddRemove(
      cliqueName: 'ghi',
      userId: 4670999,
    ));
    //Remove the busy screen.
    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
    if (base != null) {
      //Got a return data.
      _showMessage(prettyJson(base.toJson()));
    }
  }

  _testCheckToken() async {
    //Show busy screen.
    Busy.show(context);
    CheckedExpiredToken checkedExpiredToken = await checkToken();
    //Remove the busy screen.
    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
    if (checkedExpiredToken != null) {
      //Got a return data.
      _showMessage(prettyJson(checkedExpiredToken.toJson()));
    }
  }

  _testExpireToken() async {
    //Show busy screen.
    Busy.show(context);
    CheckedExpiredToken checkedExpiredToken = await expireToken();
    //Remove the busy screen.
    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
    if (checkedExpiredToken != null) {
      //Got a return data.
      _showMessage(prettyJson(checkedExpiredToken.toJson()));
    }
  }

  _testCheckTime() async {
    //Show busy screen.
    Busy.show(context);
    CheckedTime checkedTime = await checkTime();
    //Remove the busy screen.
    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
    if (checkedTime != null) {
      //Got a return data.
      _showMessage(prettyJson(checkedTime.toJson()));
    }
  }

  _testEcho() async {
    //Show busy screen.
    Busy.show(context);
    Random rand = Random();
    EchoResponse echoResponse = await echo(Echo(
      data: rand.nextInt(99999).toString(),
    ));
    //Remove the busy screen.
    Navigator.popUntil(context, (route) {
      return (route.settings.name == 'prototype');
    });
    if (echoResponse != null) {
      //Got a return data.
      _showMessage(prettyJson(echoResponse.toJson()));
    }
  }

}

// A custom colored test button.
class ColoredTestButton extends StatefulWidget {
  ColoredTestButton(this.text, this.callback);

  final String text;
  final Function callback;

  @override
  ColoredTestButtonState createState() => ColoredTestButtonState();
}

class ColoredTestButtonState extends State<ColoredTestButton> {
  final Color _buttonColor;
  ColoredTestButtonState() : _buttonColor = ColorPlus.getRandomColor();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      color: _buttonColor,
      onPressed: widget.callback,
      child: Text(
        widget.text,
        style: TextStyle(
          color: _buttonColor.suitableTextColor(),
        ),
      ),
    );
  }
}

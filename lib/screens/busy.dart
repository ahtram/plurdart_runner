import 'package:flutter/material.dart';
import 'package:flare_flutter/flare_actor.dart';

void show(BuildContext context){
  showDialog(
    barrierDismissible: false,
    routeSettings: RouteSettings(
      name: 'busy',
    ),
    context: context,
    builder: (BuildContext context) {
      return WillPopScope(
        onWillPop: () async { return false; },
        child: Material(
          color: Colors.transparent,
          child: Center(
            child:  Container(
              height: 300,
              width: 300,
              child: FlareActor("assets/rive/MeatLoader.flr",
                  animation: "Untitled",
              ),
            )
          )
        ),
      );
    },
  );
}

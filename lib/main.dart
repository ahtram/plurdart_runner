import 'package:flutter/material.dart';
import 'util/routegen.dart';

void main() {
  runApp(MaterialApp(
    initialRoute: 'prototype',
    debugShowCheckedModeBanner: false,
    onGenerateRoute: generateRoute,
  ));
}
